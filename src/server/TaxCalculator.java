package server;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface TaxCalculator extends Remote {
    int computeTax(int engineSize) throws RemoteException;

    double computeSellingPrice(double price, int year) throws RemoteException;


}
