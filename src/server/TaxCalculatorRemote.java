package server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;


public class TaxCalculatorRemote extends UnicastRemoteObject implements TaxCalculator {

    public TaxCalculatorRemote() throws RemoteException {
        super();
    }

    public int computeTax(int engineSize) {
        return (engineSize / 200) * getPartialSum(engineSize);
    }

    private int getPartialSum(int engineSize) {

        if (engineSize < 1600) return 8;
        if (engineSize < 2000) return 18;
        if (engineSize < 2600) return 72;
        if (engineSize < 3000) return 144;
        return 290;
    }

    public double computeSellingPrice(double price, int year) {
        return price - price / 7 * (2015 - year);
    }
}