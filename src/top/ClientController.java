package top;

import server.TaxCalculator;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * Created by GligaBogdan on 14-Nov-16.
 */
public class ClientController {

    private TaxCalculator taxCalculator;

    ClientController() {
        this.taxCalculator = retrieveTaxCalculator();
    }

    private TaxCalculator retrieveTaxCalculator() {
        try {
            return (TaxCalculator) Naming.lookup("rmi://localhost:5000/taxCalculator");
        } catch (NotBoundException | MalformedURLException | RemoteException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String computeSellingPrice(int carPrice, int carYear) {

        double sellingPrice;
        try {
            sellingPrice = Double.parseDouble(String.format("%.5g%n", taxCalculator.computeSellingPrice(carPrice, carYear)));
        } catch (RemoteException e) {
            e.printStackTrace();
            return "Error while computing selling price";
        }

        return "Selling price = " + sellingPrice;
    }

    public String computeTax(int engineSize) {
        double tax;
        try {
            tax = taxCalculator.computeTax(engineSize);
        } catch (RemoteException e) {
            e.printStackTrace();
            return "Error while computing selling tax";
        }

        return "Car tax = " + tax;
    }
}
