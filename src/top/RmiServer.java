package top;

import server.TaxCalculator;
import server.TaxCalculatorRemote;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

public class RmiServer {
    public static void main(String args[]) {
        try {
            LocateRegistry.createRegistry(5000);
            TaxCalculator stub = new TaxCalculatorRemote();
            Naming.rebind("rmi://localhost:5000/taxCalculator", stub);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}