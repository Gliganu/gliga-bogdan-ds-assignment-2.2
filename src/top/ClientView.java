package top;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 * Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description: ClientView is a JFrame which contains the UI elements of the Client application.
 */
public class ClientView extends JFrame {

    private ClientController clientController;
    private JTextField yearTextField;
    private JTextField engineSizeTextField;
    private JTextField priceTextField;
    private JLabel textLabel;

    public ClientView(ClientController clientController) {

        this.clientController = clientController;
        buildUI();
    }

    public void buildUI() {
        setTitle("RMI simulator");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(300, 100, 450, 300);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        int textFieldWidth = 120;

        JLabel lblFirstname = new JLabel("Car year ");
        lblFirstname.setBounds(10, 36, textFieldWidth, 14);
        contentPane.add(lblFirstname);

        JLabel lblLastname = new JLabel("Car engine");
        lblLastname.setBounds(10, 61, textFieldWidth, 14);
        contentPane.add(lblLastname);

        JLabel lblMail = new JLabel("Car price");
        lblMail.setBounds(10, 86, textFieldWidth, 14);
        contentPane.add(lblMail);

        yearTextField = new JTextField();
        yearTextField.setBounds(80, 33, textFieldWidth, 20);
        contentPane.add(yearTextField);
        yearTextField.setColumns(10);

        engineSizeTextField = new JTextField();
        engineSizeTextField.setBounds(80, 58, textFieldWidth, 20);
        contentPane.add(engineSizeTextField);
        engineSizeTextField.setColumns(10);

        priceTextField = new JTextField();
        priceTextField.setBounds(80, 83, textFieldWidth, 20);
        contentPane.add(priceTextField);
        priceTextField.setColumns(10);

        JButton computeTaxButton = new JButton("Compute Tax ");
        computeTaxButton.setBounds(80, 110, textFieldWidth, 23);
        contentPane.add(computeTaxButton);

        JButton computeSellingPriceButton = new JButton("Compute price");
        computeSellingPriceButton.setBounds(80, 150, textFieldWidth, 23);
        contentPane.add(computeSellingPriceButton);

        textLabel = new JLabel();
        textLabel.setBounds(235, 33, 171, 120);
        contentPane.add(textLabel);


        computeTaxButton.addActionListener(e -> {

            String message = "";
            try {
                int engineSize = Integer.parseInt(engineSizeTextField.getText());
                message = clientController.computeTax(engineSize);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Invalid input. Must be a number", "", JOptionPane.ERROR_MESSAGE);
            }

            displayMessage(message);
        });

        computeSellingPriceButton.addActionListener(e -> {

            String message = "";
            try {
                int carPrice = Integer.parseInt(priceTextField.getText());
                int carYear = Integer.parseInt(yearTextField.getText());

                message = clientController.computeSellingPrice(carPrice, carYear);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Invalid input. Must be a number", "", JOptionPane.ERROR_MESSAGE);
            }


            displayMessage(message);
        });

        setVisible(true);
    }

    private void displayMessage(String message) {
        textLabel.setText(message);
    }
}
